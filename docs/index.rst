through, an analysis of the Minecraft computer mods environments
================================================================

    For every outside there is an inside and for every inside there is
    an outside, and although they are different, they go together.

    -- Alan Watts, Tao of Philosophy, 1.1.8. - Myth of Myself - Pt. 2, 1995

through is an analysis of the Lua environments provided by Minecraft
computer mods such as `ComputerCraft: Tweaked`_ or OpenComputers_.

Other points of presence for through are the following:

* `The repository <https://gitlab.com/thox-project/through>`_.
* `The issues tracker <https://gitlab.com/thox-project/through/-/issues>`_, for
  reporting inaccuracies or suggesting evolutions.

This documentation is organized using `Diátaxis`_' structure.

Any question? Any issue? You can either contact me by mail at
thomas@touhey.fr or on the `Computer Mods Discord Server`_ (``@cake``).

.. toctree::
    :maxdepth: 2

    topics
    glossary
    people

.. _`ComputerCraft: Tweaked`: https://computercraft.cc/
.. _OpenComputers: https://ocdoc.cil.li/
.. _Diátaxis: https://diataxis.fr/
.. _Computer Mods Discord Server: https://discord.com/invite/MgPzSGM
