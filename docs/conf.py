"""Configuration file for the Sphinx documentation builder.

For the full list of built-in configuration values, see the documentation:
https://www.sphinx-doc.org/en/master/usage/configuration.html
"""

from __future__ import annotations

import os

project = 'through'
copyright = '2024, Thomas Touhey'
author = 'Thomas Touhey'

release = 'latest'

primary_domain = 'lua'
extensions = [
    'sphinxcontrib.luadomain',
    'sphinx.ext.imgmath',
    'sphinx.ext.todo',
]

templates_path = ['_templates']
exclude_patterns = ['_build', '**/Thumbs.db', '**/.DS_Store', '**/.*.kate-swp']

todo_include_todos = True

# HTML output options.

THUMB_URL = os.environ.get('THUMB_URL') or 'https://thox.madefor.cc/thumb/'
THOX_URL = os.environ.get('THOX_URL') or 'https://thox.madefor.cc/thox/'

html_theme = 'furo'
html_context = {
    'thumb_url': THUMB_URL,
    'thox_url': THOX_URL,
}
html_title = 'through, an analysis of the Minecraft computer mods environments'
html_favicon = 'favicon.png'
html_logo = '_static/simple_logo.svg'
html_extra_path = ['robots.txt', 'favicon.ico']
html_math_renderer = 'mathjax'
