.. _randomness:

Randomness and entropy
======================

.. todo::

    Describe the following problematics:

    * Standard random utilities.
    * Entropy sources available from a thox computer.
    * How pseudorandom can be used to extend the number of bytes
      generated using a given entropy.

.. todo::

    Reformulate elements from the OneOS protocols page:

    ComputerCraft implements :lua:func:`math.random` on top of the
    `Cobalt implementation of math.random`_, which in turn depends
    on `java.util.Random`_.

    By default, this utility requires a seed set using
    :lua:func:`math.randomseed`, which “sets the seed of the random number
    generator to a value very likely to be distinct from any other
    invocation of this constructor”, `currently System.nanoTime()
    <java.util.Random uses System.nanoTime()_>`_.

.. _java.util.Random: https://docs.oracle.com/javase/8/docs/api/java/util/Random.html
.. _Cobalt implementation of math.random: https://github.com/SquidDev/Cobalt/blob/master/src/main/java/org/squiddev/cobalt/lib/MathLib.java#L191
.. _`java.util.Random uses System.nanoTime()`: https://stackoverflow.com/a/20060872
