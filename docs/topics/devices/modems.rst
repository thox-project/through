.. _modem-devices:

Modem devices
=============

Computers in general can network through various ways. In ComputerCraft and
OpenComputers, although other means such as redstone or other mods exist in
order to make two computers communicate, the most common one is modems.

A **modem** is a device that allows **hosts** to send **messages**, as
datagrams, on given **ports** (also named channels). These messages can either
be directed at given computers, which is called "addressing", or broadcasted.

A port can have different formats, specific to the modem type, and there
can be a maximal number of opened ports by modem.

Messages can have varying types according to the modem type. Some examples
are:

* ComputerCraft modem messages can be a string, a number, or a table
  combining these types.
* Skynet modem messages can be any message CBOR-encodable.

See the following documents for specific modem definitions.

.. toctree::
    :maxdepth: 1

    modems/computercraft
    modems/opencomputers
    modems/skynet
    modems/rednoot
