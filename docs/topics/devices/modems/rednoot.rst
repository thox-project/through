rednoot modems
==============

.. sidebar:: rednoot

    .. list-table::
        :widths: 2 8

        * - addressing
          - no
        * - max_open
          - none
        * - port_fmt
          - :math:`[1, 65535]`
        * - msg_fmt
          - any string, number, boolean or table

`rednoot`_ (`thread <rednoot on ComputerCraft forums_>`_) is a virtual modem
running on top of websockets. The project was started in 2017 by
:ref:`player-justync7`; it is no longer maintained, and the
public instance recommended by the repo is no longer available, although
the source code for both the client and the server are published under MIT,
along with its `protocol specification <rednoot specification_>`_.

The virtual modem behaves like a ComputerCraft modem, except that:

* It is inter-platform: computers can communicate no matter the distance,
  across servers, and even with other devices on the Internet; however, note
  that from a security perspective, making your devices communicating with a
  server you own directly is better.
* There is no arbitrary limit to the number of channels a host can open.
* Modem channels are numbers between 1 and 65535; channel number 0 isn't
  a valid channel like for ComputerCraft modems, which can be a problem when
  using Rednet with rednoot since 0 is the first computer ID attributed on
  any world save; however, a client is attributed an identifier when
  connecting, so using the local computer identifier is discouraged when
  using rednoot.

Some remarks about the protocol itself described in the specification:

* Messages are exchanged as stringified JSON documents, which the length as
  a number of characters (not bytes) must not exceed 65535 characters
  (resulting in a ``message_too_long`` error) and for which the root
  element of the parsed document must be a JSON array, other types resulting
  in an ``invalid_type`` error.
* When connecting to a rednoot network, clients are attributed an identifier
  proper to the modem instead of using their computer ID. This is an
  efficient way to get an identifier that is guaranteed to be unique
  network-wide. This identifier is a number comprised between 1 and 65535;
  it can therefore be used for Rednet-like communications.
* When sending messages, the client receives no acknowledgment but might
  receive an error message, without the message identifier. This is only
  useful when debugging an implementation manually.

See the `rednoot client script`_ and the `rednoot message callback
function`_ for reference.

.. _rednoot: https://github.com/Lustyn/rednoot
.. _rednoot specification: https://github.com/Lustyn/rednoot/blob/master/SPECIFICATION.md
.. _rednoot message callback function: https://github.com/Lustyn/rednoot/blob/e29b30546d59ddda475509f5d272ed8eaa0b3396/app.js#L158
.. _rednoot client script: https://github.com/Lustyn/rednoot/blob/e29b30546d59ddda475509f5d272ed8eaa0b3396/client.lua
.. _rednoot on ComputerCraft forums: http://www.computercraft.info/forums2/index.php?/topic/28595-rednoot-a-global-rednetmodem-bridge/
