.. _computers:

Computers
=========

A computer is a machine executing Lua code as machine code.
Computers officially supported for thox are provided by the following
projects:

* Computers and other devices running on Minecraft mod `ComputerCraft`_,
  and its maintained fork, `ComputerCraft: Tweaked`_ (abbreviated CC:T).
* CC:T emulator `CCEmuX`_.
* CC:T emulator `CraftOS-PC`_.

.. note::

    While :ref:`player-dan200` started the original `ComputerCraft`_ mod in
    2011, he officially released the source code to version 1.79 on May 1st,
    2017 so he could "devote time to other projects";
    see commit `ComputerCraft 1.79 initial upload`_.

    It is now unmaintained, the community now mainly using CC:T,
    the fork made and maintained to this day by :ref:`player-squiddev`.

Supported versions of Lua are versions 5.1 to 5.3.
This Lua runtime, with additional features specific to each computer, is
provided by a multitude of projects:

* Original ComputerCraft used a private fork of `LuaJ`_ version 2.
  It implements Lua 5.1.
* ComputerCraft:Tweaked (CC:T) and `CCEmuX`_ use `Cobalt`_, a
  fork of `LuaJ`_ version 2 by the same author. It implements
  Lua 5.1, and has "many features of LuaJ 3.0 (Lua 5.2) backported".
* `CraftOS-PC`_ uses `CraftOS2-Lua`_, a fork of original Lua
  implementation version 5.1.
* OpenComputers uses `OC-LuaJ`_, a fork of `LuaJ`_ version 3.

There are several computer types available through these mods:

* Basic and advanced computers, which are basic computers occupying a block
  in the Minecraft world.
* Command computers, which are advanced computers able to execute server
  commands, alike command blocks.
* Turtles, which are computers able to move and interact more with the
  surrounding world.
* Basic and advanced pocket computers, which are computers that work as
  items.

These different computers vary in the devices they support; see
:ref:`bus-core` to see the possibilities.

.. _ComputerCraft: http://www.computercraft.info/
.. _`ComputerCraft: Tweaked`: https://computercraft.cc/
.. _CCEmuX: https://emux.cc/
.. _CraftOS-PC: https://www.craftos-pc.cc/
.. _LuaJ: http://www.luaj.org/luaj/3.0/README.html
.. _Cobalt: https://github.com/cc-tweaked/Cobalt
.. _CraftOS2-Lua: https://github.com/MCJack123/craftos2-lua
.. _OC-LuaJ: https://github.com/MightyPirates/OC-LuaJ
.. _ComputerCraft 1.79 initial upload: https://github.com/dan200/ComputerCraft/commit/e85cdacbc58dedacb7fcbb119efd2b44db4bcdd6
