.. _modem-phoenix:

Phoenix protocols
=================

Phoenix_ is an operating system made by :ref:`player-jackmacwindows`.
As described in :ref:`modem-phoenix-networking`, it implements custom
networking akin to Rednet, with more layers in order to support inter-network
communication inspired from TCP/IP.

To the user, as described in the `Networking API`_, this is abstracted as a
connected socket abstraction for both connected and disconnected transport
protocols.

The layers are the following:

+-------------+--------------------------------------------------------------------------------+
| Layer       | Protocols                                                                      |
+=============+==========================+==========================+==========================+
| Application |                          |                          | ...                      |
+-------------+                          |                          +--------------------------+
| Transport   |                          | :ref:`modem-phoenix-mcp` | :ref:`modem-phoenix-psp` |
+-------------+                          +--------------------------+--------------------------+
| Network     |                          | :ref:`modem-phoenix-pip`                            |
+-------------+                          +-----------------------------------------------------+
| Link        | :ref:`modem-phoenix-arp` | :ref:`modem-phoenix-plp`                            |
+-------------+--------------------------+-----------------------------------------------------+

See the following sections for details regarding every protocol.

.. _modem-phoenix-arp:

Address Resolution Protocol (Phoenix ARP)
-----------------------------------------

`Address Resolution Protocol`_ (ARP) in Phoenix is a protocol for resolving
IP addresses into computer IDs. It takes place on channel 0.

Packets in this protocol are tables with values depending on whether the
packet is an ARP request or an ARP reply.

ARP requests have the following keys:

    ``PhoenixNetworking`` (req.)
        Identifies a Phoenix Networking packet. Set to ``true``.

    ``type`` (req.)
        Identifies a Phoenix ARP packet. Set to ``"arp"``.

    ``reply`` (req.)
        Identifies a Phoenix ARP request, set to ``false``.

    ``source`` (req.)
        Identifier of the source host, as a number.

    ``sourceIP`` (opt.)
        IP address of the source host, as a string.

    ``destinationIP`` (req.)
        IP address of the destination host, as a string.

ARP replies have the following keys:

    ``PhoenixNetworking`` (req.)
        Identifies a Phoenix Networking packet. Set to ``true``.

    ``type`` (req.)
        Identifies a Phoenix ARP packet. Set to ``"arp"``.

    ``reply`` (req.)
        Identifies a Phoenix ARP reply, set to ``true``.

    ``source`` (req.)
        Identifier of the source host, as a number.

    ``sourceIP`` (req.)
        IP address of the source host, as a string.

    ``destination`` (req.)
        Identifier of the destination host, as a number.

        This is a copy of the ``source`` field from the request.

    ``destinationIP`` (opt.)
        IP address of the destination host, as a string.

        This is a copy of the ``sourceIP`` field from the request, if present.

When receiving an ARP request, a host must reply if the following conditions
are met:

* ``destinationIP`` corresponds to the IP address of the interface or device
  on which the ARP request has been received.
* Either ``sourceIP`` if unset, or is different from ``destinationIP``.

The reply must copy ``source`` and ``sourceIP`` into the ``destination`` and
``destinationIP`` fields respectively, and ``source`` and ``sourceIP`` must
be set to the current host's numerical identifier and IP address.

This protocol must **only** be used for one use case: resolve an IP address
into a numerical host identifier. It **cannot** be used to discover all
neighbours and their IP addresses\ [#arp_neigh]_.

.. _modem-phoenix-plp:

Phoenix Link Protocol (PLP)
---------------------------

`Phoenix Link Protocol`_ (PLP) is a protocol for sending data from a computer
to another, over a direct modem message, including channel 0.
It identifies computers using numbers, determined with `os.getComputerID`_.

Packets in this protocol are tables with the following properties:

    ``PhoenixNetworking`` (req.)
        Identifies a Phoenix Networking packet. Set to ``true``.

    ``type`` (req.)
        Identifies a PLP packet. Set to ``"link"``.

    ``source`` (req.)
        Identifier of the source host, as a number.

    ``destination`` (opt.)
        Identifier of the destination host, as a number.

        If absent, the message is a broadcast message.

    ``payload``
        Payload of the protocol above, assumed to contain at least the
        following properties:

        ``PhoenixNetworking``
            Identifiers a Phoenix Networking packet. Set to ``true``.

        ``type``
            Protocol or type of the carried payload.

This protocol is akin to :ref:`rednet`.

.. _modem-phoenix-pip:

Phoenix Internet Protocol (PIP)
-------------------------------

`Phoenix Internet Protocol`_ (PIP) is protocol implementing a
`packet-switched network`_ over :ref:`modem-phoenix-plp`.
It allows sending datagrams_ from one host to another, representing hosts
as IPv4-like addresses.

Packets in this protocol are tables with the following properties:

    ``PhoenixNetworking`` (req.)
        Identifies a Phoenix Networking packet. Set to ``true``.

    ``type`` (req.)
        Identifies a PIP packet. Set to ``"internet"``.

    ``messageID``
        Identifier for the message, as a string or number.

        This allows discarding duplicate messages.

    ``source`` (req.)
        IP address of the host sending a message.

    ``destination`` (req.)
        IP address of the host which should receive the message.

    ``hopsLeft`` (req.)
        Number of hops left.

    ``payload`` (req.)
        Payload of the protocol above, assumed to contain at least the
        following properties:

        ``PhoenixNetworking``
            Identifiers a Phoenix Networking packet. Set to ``true``.

        ``type``
            Protocol or type of the carried payload.

In its simplest version, upon receiving a PIP packet, a host should read
the payload if the ``destination`` corresponds to the host's IP address,
and discard it otherwise.

In order to know to which host to send an IP packet, i.e. determine the next
"hop", a host must have a routing table with rules depending on the
destination and source. You can inspire yourself from Phoenix's routing table
format with routelist_.

.. _modem-phoenix-mcp:

Message Control Protocol (Phoenix MCP)
--------------------------------------

`Message Control Protocol`_ (MCP) in Phoenix is a protocol for determining
if a host with a given IP address is up and accessible, or not.

Packets in this protocol are tables with the following properties:

    ``PhoenixNetworking`` (req.)
        Identifies a Phoenix Networking packet. Set to ``true``.

    ``type`` (req.)
        Identifies an MCP packet. Set to ``"control"``.

    ``messageType`` (req.)
        Message type, as a string among the following:

        ``"ping"``
            Indicates a ping request, which should be replied with a
            subsequent ``"pong"`` message.

        ``"pong"``
            Indicates a reply to a previous ``ping`` request.

            .. note::

                If the original value from the destination host is known,
                ``hopsLeft`` from the surrounding PIP packet can help the host
                that emitted the ping determine the number of hosts.

                By default, Phoenix uses 15 hops on its PIP packets.

        ``"unreachable"``
            Indicates that a previous message was unable to be sent to the
            destination.

        ``"timeout"``
            Indicates that a previous message was rerouted too many times,
            and was subsequently rejected.

    ``error`` (opt.)
        If set, can be a string providing additional information on why
        the message was sent.

        This is targeted towards developers, and should not be parsed.

In its simplest form, upon receiving an MCP ``"ping"`` packet, a host should
answer with an MCP ``"pong"`` packet.

.. _modem-phoenix-psp:

Phoenix Socket Protocol (PSP)
-----------------------------

`Phoenix Socket Protocol`_ (PSP) is a protocol for establishing a connection
over :ref:`modem-phoenix-pip`.

.. todo:: Write this!

.. [#arp_neigh] Unless you make an ARP request for every possible IPv4 address.

.. _Phoenix: https://phoenix.madefor.cc/
.. _Networking: https://phoenix.madefor.cc/docs/network
.. _Networking API: https://phoenix.madefor.cc/syscalls/network
.. _Phoenix Link Protocol:
    https://phoenix.madefor.cc/docs/network#phoenix-link-protocol-plp
.. _Address Resolution Protocol:
    https://phoenix.madefor.cc/docs/network#address-resolution-protocol
.. _Phoenix Internet Protocol:
    https://phoenix.madefor.cc/docs/network#phoenix-internet-protocol-pip
.. _routelist:
    https://phoenix.madefor.cc/syscalls/network#routelistnum-number-table
.. _Message Control Protocol:
    https://phoenix.madefor.cc/docs/network#message-control-protocol
.. _Phoenix Socket Protocol:
    https://phoenix.madefor.cc/docs/network#phoenix-socket-protocol-psp

.. _os.getComputerID: https://tweaked.cc/module/os.html#v:getComputerID
.. _packet-switched network:
    https://en.wikipedia.org/wiki/Packet-switched_network
.. _datagrams: https://en.wikipedia.org/wiki/Datagram
