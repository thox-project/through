gollark's modem protocols
=========================

:ref:`player-gollark` has a few protocols of their own, either running on
vanilla CraftOS or on PotatOS.

.. _modem-gollark-lms:

Lightweight Messaging System
----------------------------

`Lightweight Messaging System`_ is a modem protocol running on
port 3636. It is an unauthenticated, unencrypted protocol to exchange simple
messages between computers, usually players.

LMS packets consist of a table like:

.. code-block:: lua

    { message = "message", username = "username" }

Where the message and username are nominally strings (though this is not
validated). Messages are clipped to 128 characters (in the ComputerCraft
character encoding, :ref:`computercraft-encoding`) and scrubbed of some banned
content in the official client, though usernames are not.

The official LMS client also transmits a "Connected." message on startup.

.. _modem-gollark-lancmd:

PotatOS LANcmd
--------------

`PotatOS LANcmd <lancmd_>`_ is a modem protocol intended for management of
PotatOS networks. It allows exactly two messages, ``update`` and ``shutdown``,
which trigger an update and shutdown respectively. It uses port 62381.

.. _modem-gollark-rift:

Rift
----

Rift_ is a protocol and program for coordinating Mekanism_ teleporters
(normally point-to-point) to function as an any-to-any system. It runs over
port 31415.

The protocol consists of ordered tables. These can represent either a
``command``, ``result`` or ``error``. By default, these are not addressed:
a command is processed by all listening Rift devices and there is no way to
track what caused an error.

An ``error`` message contains a string representation of the error, and
``result`` contains various types depending on the command in use.

Available commands are the following:

``ping``
    The result is a table containing only the "rift ID" (label) of the receiver.

``update``
    Trigger an update from Pastebin; no response, as this triggers a reboot.

``dial``
    Takes a "from" rift ID and "to" rift ID, and is processed only by the
    matching computer, if any - it sets the frequency of the teleporter to
    one which should also be used by the remote, linking them),
    and ``handle_disconnect``, which sets the teleporter to a "null" frequency
    which should be unused (to be used when the remote initiates a disconnect).

.. _modem-gollark-adlcp:

Apiaristics Division Laser Coordination Protocol
------------------------------------------------

`Apiaristics Division Laser Coordination Protocol`_ is a protocol for
controlling a `laser <Plethora laser_>`_ and `sensor <Plethora sensor_>`_ pair
from Plethora_. It uses channel 55 on ComputerCraft modems.

.. todo:: Write this!

.. _modem-gollark-endermail:

EnderMail
---------

EnderMail_ is a system and protocol for sending and receiving messages using
ender chests.

.. todo:: Write this!

.. _modem-gollark-lasertbm:

Laser TBM Coordination Protocol
-------------------------------

`Laser TBM Coordination Protocol`_ is a protocol for controlling a
`laser <Plethora laser_>`_ from Plethora_.

.. todo:: Write this!

.. _modem-gollark-basenet:

BaseNet
-------

BaseNet_ is an asynchronous and distributed task execution protocol.
It uses channel 23032 by default on ComputerCraft modems.

.. todo:: Write this!

.. _Lightweight Messaging System: https://pastebin.com/L0ZKLBRG
.. _lancmd:
    https://git.osmarks.net/osmarks/potatOS/src/branch/master/src/main.lua#L1350
.. _Rift:
    https://github.com/osmarks/misc/blob/
    acccb2f7157042ccaf80eaed6e2b1cfce3a778a3/computercraft/rift.lua
.. _Mekanism: https://wiki.aidancbrady.com/wiki/Main_Page
.. _Apiaristics Division Laser Coordination Protocol:
    https://github.com/osmarks/misc/blob/
    acccb2f7157042ccaf80eaed6e2b1cfce3a778a3/computercraft/panel-turtle.lua#L93
.. _EnderMail:
    https://github.com/osmarks/misc/blob/
    acccb2f7157042ccaf80eaed6e2b1cfce3a778a3/computercraft/endermail.lua
.. _Laser TBM Coordination Protocol:
    https://github.com/osmarks/misc/blob/
    acccb2f7157042ccaf80eaed6e2b1cfce3a778a3/computercraft/laser_tbm.lua
.. _BaseNet:
    https://github.com/osmarks/misc/blob/
    acccb2f7157042ccaf80eaed6e2b1cfce3a778a3/computercraft/basenet.lua

.. _Plethora: https://plethora.madefor.cc/
.. _Plethora laser: https://plethora.madefor.cc/items/module-laser.html
.. _Plethora sensor: https://plethora.madefor.cc/items/module-sensor.html
