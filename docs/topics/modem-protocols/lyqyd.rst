LyqydNet-related modem protocols
================================

From 2012 to 2017, :ref:`player-lyqyd` has made a set of network solutions
using a common set of solutions named "LyqydNet", notably used in `CC-Hive`_.

.. todo::

    Similarities with Rednet, e.g. using computer IDs (with limits!).
    What ports is it using? Not actually sure

    Dissimilarities with Rednet, e.g. no repeat port, etc.

    Transport AND application protocol... bad, really.

See the `main repository`_ and the `net daemon source code`_
for more information.

Packet format
-------------

Packets are tables containing the following fields:

.. list-table::
    :widths: 1 1 10
    :header-rows: 1

    * - Attribute
      - Presence
      - Role
    * - ``lyqydnet``
      - Required
      - Always set to ``true`` (or any value evaluated as ``true``).
        Serves for isolating LyqydNet-specific packets.
    * - ``destination``
      - Required
      - Set to the destination host's numerical computer identifier.
    * - ``source``
      - Required?
      - Set to the source host's numerical computer identifier.
    * - ``type``
      - Required.
      - Set to a two-character identifier for the packet's purpose.
    * - ``toSock``
      - ?
      - A numerical identifier of the destination socket (equivalent
        to a "port" in more traditional connected transport protocols).
    * - ``fromSock``
      - ?
      - A numerical identifier of the source socket (equivalent
        to a "port" in more traditional connected transport protocols).
    * - ``confirm``
      - ?
      - An acknowledgement request flag which makes the current
        host repeat the message to the sender with the ``payload`` set to the
        current message and the ``type`` set to ``PS``.

Packet types are the following:

.. list-table::
    :widths: 1 1 10
    :header-rows: 1

    * - Code
      - Family
      - Role
    * - ``SQ``
      - Socket
      - A query?
    * - ``SR``
      - Socket
      - A response?
    * - ``SP``
      - Socket
      - A data packet?
    * - ``SI``
      - Socket
      - An instruction?
    * - ``SB``
      - Socket
      - "done"?
    * - ``SC``
      - Socket
      - "close"?
    * - ``PS``
      - Packet
      - A packet acknowledgement, or "packet success",
        returning on ``confirm == true``?
    * - ``HA``
      - Hosts
      - Add a host route?
    * - ``HT``
      - Hosts
      - Send a hosts table?
    * - ``HI``
      - Hosts
      - A host sends its own information.
    * - ``HC``
      - Hosts
      - A host saves a Cost?
    * - ``HQ``
      - Hosts
      - Broadcasting ``HA``?
    * - ``HR``
      - Hosts
      - Remove a route.
    * - ``RM``
      - Rednet
      - A Rednet message to be treated as Rednet message?

Socket instruction (``SI``) packet payloads are the following:

.. list-table::
    :widths: 1 10
    :header-rows: 1

    * - Code
      - Role
    * - ``announce``
      - Announce ourselves as ``HA`` packet using
        ``{label = <our label>, type = T or R or C}``.
    * - ``start``
      - ?
    * - ``route``
      - Enable routing?
    * - ``longlink``
      - Enable longlink mode?
    * - ``turtlepacket``
      - ?

.. _CC-Hive: https://github.com/CC-Hive/Main
.. _main repository: https://github.com/lyqyd/ComputerCraft-LyqydNet
.. _net daemon source code: https://github.com/lyqyd/ComputerCraft-LyqydNet/blob/01b471fdaa18bcc69a96e2b59a04604c66f4ec02/bin/lyqydnet#L382
