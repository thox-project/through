JackMac's protocols
===================

:ref:`player-jackmacwindows` is the author of the `CraftOS-PC`_ emulator, and
a number of snippets and programs as proof of concepts which he publishes
as gists_.

In this document, we'll describe protocols of some of their software.

.. _modem-jackmac-rawshell:

rawshell protocols
------------------

rawshell_ is an encrypted KVM protocol, announced as follows by its creator:

    I've made a new remote shell program for ComputerCraft. Currently called
    "rawshell" (name WIP), it allows you to host a shell server on a computer
    that other computers can connect to over a modem, allowing those computers
    to run programs on the server and see the output.

    Unlike the aging nsh program it replaces, rawshell features end-to-end
    encryption, backgrounding support, built-in password authentication, and
    custom launch programs/shells.

    Support for file transfer is in progress as well.

    -- JackMacWindows on the Computer Mods Discord Server, June 6th, 2021

It uses a port protocol on port 5731 to setup a connection, then redirects
connections on any port provided to the server through the command-line.

.. todo:: Describe the protocol.

.. _CraftOS-PC: https://www.craftos-pc.cc/
.. _gists: https://gist.github.com/MCJack123
.. _rawshell: https://gist.github.com/MCJack123/8c8861e5e3082d2bed18d07641b5b2cc
