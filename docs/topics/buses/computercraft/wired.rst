.. _bus-computercraft-wired:

ComputerCraft wired bus
-----------------------

Peripherals can also be attached to a wired network through a wired modem,
not necessarily the one that is also connected to the computer. In the
following configuration, the computer can access a printer and a monitor
through the wired modem at its back:

.. image:: wired-example.png
    :alt: A computer is connected to a printer through a wired modem, and
          to a monitor through another wired modem, the two wired modems
          being connected by a network cable.

In order to access the devices connected to the wired bus, one must use the
peripheral methods of the modem (see the modem_ peripheral API), namely:

* `modem.getNamesRemote`_ to list the peripherals.
* `modem.getMethodsRemote`_ to get the methods.
* `modem.callRemote`_ to call a remote method.

Computers connected to a wired network will, for one event emitted by a device
on the wired network, receive this event as many times as it is connected to
the network. For example, this computer will receive the events emitted by
the monitor twice:

.. image:: wired-twice.png
    :alt: A computer connected through two wired modems to the same wired
          network.

.. _modem: https://tweaked.cc/peripheral/modem.html
.. _peripheral.getNames: ttps://github.com/cc-tweaked/CC-Tweaked/blob/1f70ed69854de12356681f4036458894a5848c0d/src/main/resources/data/computercraft/lua/rom/apis/peripheral.lua#L27
.. _modem.getNamesRemote: https://tweaked.cc/peripheral/modem.html#v:getNamesRemote
.. _modem.getMethodsRemote: https://tweaked.cc/peripheral/modem.html#v:getMethodsRemote
.. _modem.callRemote: https://tweaked.cc/peripheral/modem.html#v:callRemote
