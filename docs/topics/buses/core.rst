.. _bus-core:

Core bus
========

The core bus is an abstraction of native platform-specific APIs, through
device abstractions representing them. It is also the root for all buses
and devices.

On ComputerCraft, computers can interact with Minecraft and the outside
worlds using those device abstractions on the core bus:

* An **internal hard drive**, which cannot be removed. This device is
  interacted with in vanilla CraftOS using the `fs`_ API.
* **Mouse, keyboard and clipboard** when the device is selected.
  These devices only interact with the computers by creating events,
  such as the `key event`_, the `mouse_click event`_ or the `paste event`_.
* **Monochrome** (for basic computers and turtles) **and 4-bit color**
  (for advanced and command computers and turtles) **integrated character
  displays**. This device is interacted with in vanilla CraftOS using
  the `term`_ API.
* **Turtle members**, which allows the computer to move and interact with the
  world and an inventory (for basic and advanced turtles). This device is
  interacted with in vanilla CraftOS using the `turtle`_ API.
* **Commands emitting module** (for command computers), which allows the
  computer to break the fourth wall and interact directly with the Minecraft
  server. This device is interacted with in vanilla CraftOS using the
  `commands`_ API.
* **Redstone enablers** on each side of the computer, which allows the
  computer to send a redstone signal with a given power on each of the six
  sides of the computer. This device is interacted with in vanilla CraftOS
  using the `redstone`_ API.
* **HTTP requester** (if enabled in the configuration), which allows the
  computer to interact with web servers on the real Internet by making
  HTTP requests. This device is interacted with in vanilla CraftOS using
  the `http`_ API.
* **Websocket manager** (if enabled in the configuration), which allows the
  computer to create websocket connections with servers on the real Internet.
  This device is interacted with in vanilla CraftOS using the
  `http.websocket`_ function.

.. _fs: https://tweaked.cc/module/fs.html
.. _key event: https://tweaked.cc/event/key.html
.. _mouse_click event: https://tweaked.cc/event/mouse_click.html
.. _paste event: https://tweaked.cc/event/paste.html
.. _term: https://tweaked.cc/module/term.html
.. _turtle: https://tweaked.cc/module/turtle.html
.. _commands: https://tweaked.cc/module/commands.html
.. _redstone: https://tweaked.cc/module/redstone.html
.. _http: https://tweaked.cc/module/http.html
.. _http.websocket: https://tweaked.cc/module/http.html#v:websocket
