.. _bus-opencomputers:

OpenComputers bus
=================

The OpenComputers bus is described in `Component Access`_ in the OpenComputers
documentation. It is a universal bus on OpenComputers, which addresses all
devices using UUIDs.

.. todo::

    Describe how the bus works, and do addresses actually affect signals,
    being the events in OpenComputers.

    For what I've seen for now, most signals (but not all) use the related
    component address as the first argument; not all, however.

.. todo::

    I thought there was a complete abstraction of the physical layout
    of the bus, but there appears to be a "slot" notion. What is that
    and what does it represent exactly?

See the `Component API`_ for reference.

.. _Component Access: https://ocdoc.cil.li/component:component_access
.. _Component API: https://ocdoc.cil.li/api:component
.. _Signals: https://ocdoc.cil.li/component:signals
