through, an analysis of the Minecraft computer mods environments
================================================================

through is an analysis of the Lua environments provided by Minecraft
computer mods such as `ComputerCraft: Tweaked`_ or OpenComputers_.

For more information, consult the following references:

* `Documentation <https://thox.madefor.cc/through/>`_ (see the ``docs/``
  directory for sources).
* `Issues <https://gitlab.com/thox-project/through/-/issues>`_.

Any question? Any issue? You can either contact me by mail at
thomas@touhey.fr or on the `Computer Mods Discord Server`_ (``@cake``).

.. _`ComputerCraft: Tweaked`: https://computercraft.cc/
.. _OpenComputers: https://ocdoc.cil.li/
.. _Computer Mods Discord Server: https://discord.com/invite/MgPzSGM
